import java.util.Scanner;

public class CalcIMC {

    public static void main(String[] args) {

        System.out.println("|---- Calculadora de IMC ----|");
        System.out.println("|     Peso: __,__ Kg         |");
        System.out.println("|   Altura: __,__ m          |");
        System.out.println("|      IMC: __.__ kg/m2      |");
        System.out.println("|----------------------------|");

        Pessoa pessoa = new Pessoa();

        Scanner nome = new Scanner (System.in);
        Scanner peso = new Scanner (System.in);
        Scanner altura = new Scanner (System.in);

        System.out.print("Qual é o seu nome?: ");
        pessoa.nome = nome.nextLine();

        System.out.print("Olá " + pessoa.nome + " qual é o seu peso?: ");
        pessoa.peso = peso.nextDouble();

        System.out.print("E qual é a sua altura?: ");
        pessoa.altura = altura.nextDouble();

        pessoa.calcularIMC();

        System.out.println(pessoa.nome + " seu IMC atual é: " + pessoa.imc);


    }

}

