import java.util.Scanner;
public class Menu {

        public static void main(String[] args) {
            Scanner menu = new Scanner(System.in);
            int opcao;
            do {
                // Criar layout do Menu de Opções
                System.out.println("|----- MENU DE OPÇÕES -----|");
                System.out.println("|Escolha uma Opção:        |");
                System.out.println("|                          |");
                System.out.println("|       1) Opção 1         |");
                System.out.println("|       2) Opção 2         |");
                System.out.println("|       3) Sair            |");
                System.out.println("|__________________________|");

                System.out.print("Escolha uma opção: ");

                opcao = menu.nextInt();

                escolha (opcao);

            } while(opcao != 3);

        }

        public static void escolha (int opcao){

            switch (opcao) {

                case 1 -> {
                    System.out.println("Você escolheu a primeira opção");

                }
                case 2 -> {
                    System.out.println("Você escolheu a segunda opção");

                }
                case 3 -> {
                    System.out.println("O programa foi encerrado");

                }
                default -> {
                    System.out.println("Você escolheu uma opção inválida");

                }
            }

        }

    }
